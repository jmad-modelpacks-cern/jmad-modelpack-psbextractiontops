package jmad.modelpack.psbextractiontops.modeldef;

import cern.accsoft.steering.jmad.domain.ex.JMadModelException;
import cern.accsoft.steering.jmad.domain.optics.Optic;
import cern.accsoft.steering.jmad.gui.JMad;
import cern.accsoft.steering.jmad.gui.JMadGui;
import cern.accsoft.steering.jmad.model.JMadModel;
import cern.accsoft.steering.jmad.modeldefs.domain.JMadModelDefinition;
import cern.accsoft.steering.jmad.modeldefs.domain.OpticsDefinition;
import cern.accsoft.steering.jmad.service.JMadService;
import org.apache.log4j.Logger;

import java.io.File;
import java.util.List;

import static org.junit.Assert.assertNotNull;

public class JMadBrowserTest {

    private static final Logger LOGGER = Logger.getLogger(JMadBrowserTest.class);

    public static void main(String[] args) throws JMadModelException {

        JMad jmad = JMad.createStandaloneJMad();
        JMadModelDefinition jMadModelDefinition = jmad.getJMadService().getModelDefinitionImporter().importModelDefinition(new File("src/java/jmad/modelpack/psbextractiontops/modeldef/psbtops.jmd.xml"));
//        jmad.getJMadGui().showGui();

        testOpenModel(jmad.getJMadService(), jMadModelDefinition);
    }

    public static void testOpenModel(JMadService jmadService, JMadModelDefinition modelDefinition) throws JMadModelException {
        System.out.println("");
        System.out.println("==== [START] Testing model '" + modelDefinition + "'. ====");
        assertNotNull("Model definition must not be null", modelDefinition);

        /* create the model */
        JMadModel model = jmadService.createModel(modelDefinition);
        assertNotNull("The created model must not be null.", model);
        System.out.println("Model '" + model + "' successfully created.");

        /* init the model */
        model.init();
        System.out.println("Model '" + model + "' successfully initialized.");

        /* test all optics definitions and recalculation of the optic */
        for (OpticsDefinition opticsDefinition : modelDefinition.getOpticsDefinitions()) {
            model.setActiveOpticsDefinition(opticsDefinition);
            Optic optic = model.getOptics();
            assertNotNull("Optic must not be null", optic);
            System.out.println("Optics values for optic '" + opticsDefinition + "' successfully retrieved.");
        }

        /* and close it again */
        model.cleanup();
        System.out.println("Model '" + model + "' successfully cleaned.");

        /* and remove it from the manager */
        jmadService.getModelManager().removeModel(model);
        System.out.println("==== [FINISHED] Testing model '" + modelDefinition + "'. ====");
        System.out.println("");
    }
}
