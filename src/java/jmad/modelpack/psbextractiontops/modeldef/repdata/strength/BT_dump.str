option, -echo;
option, warn;
option, info;

!--------------------------------------------------------------------------------------
! - Strength for beam dumping
!
! - Gradients of QNO according to report from C.Carli (Qv=4.23):
!   http://cern-accelerators-optics.web.cern.ch/cern-accelerators-optics/BTBTM/LowWP.ps
!
! - Bending angle for septum, bending, kicker and correctors obtained by matching.
!   Except the angles of the BTV10s and BVT20 are determined from the trajectories described
!   in the report PS/OP/BR/Note 81-5 from J.P. Delahaye and J.P. Rinaud
!--------------------------------------------------------------------------------------
! Directory: /afs/cern.ch/eng/ps/cps/TransLines/PSB-PS/2013
! Strength file created in August 2012 by V.Raginel, O.Berrig and B. Mikulec
!--------------------------------------------------------------------------------------


/****************************************************************************
 * BT1 + BT4
 ****************************************************************************/
! dBTBVT10 is based on the report PS/OP/BR/Note 81-5 from J.P. Delahaye and J.P. Rinaud
! dBTBVT10 = atan((.36-.063)/(BT4.SMV10.mechpos-BT4.BVT10.mechpos)); ! = 76.791702273e-3
 dbt1bvt10 = 0.076813708400 ;
 dbt4bvt10 = 0.076813708400 ;
 dbt1smv10 = 0.073574303870 ;
 dbt4smv10 = 0.073574303870 ;
 dbt1qno10 = 0.009859945065 ;
 dbt4qno10 = 0.009859945065 ;
 dbt1qno20 = 0.004532373022 ;
 dbt4qno20 = 0.004532373022 ;
 dbt1kf10  = 0.008566976581 ;
 dbt4kf10  = 0.008566976581 ;
 dBT1DHZ10 = 0;
 dBT2DHZ10 = 0;

/****************************************************************************
 * BT2 + BT3
 ****************************************************************************/
 dbt2dvt10 = 0.004625317966 ;
 dbt3dvt10 = 0.004625317966 ;
 dbt2dvt20 = 0.010437889850 ;
 dbt3dvt20 = 0.010437889850 ;
 dbt2qno10 = 0.009972548092 ;
 dbt3qno10 = 0.009972548092 ;
 dbt2qno20 = 0.004159976211 ;
 dbt3qno20 = 0.004159976211 ;
 DBT2DHZ10 = 0;
 DBT3DHZ10 = 0;

/****************************************************************************
 * BT2 + BT1
 ****************************************************************************/
! dBTBVT20 is based on the report PS/OP/BR/Note 81-5 from J.P. Delahaye and J.P. Rinaud
! dBTBVT20 =  atan((.36-.044)/(BT2.SMV20.mechpos-BT2.BVT20.mechpos));
 dbtbvt20  = 0.074184999920 ;
 dbtsmv20  = 0.071321185570 ;
 dbt3qno30 = 0.002832646832 ;
 dbt2qno30 = 0.002548495587 ;
 dbtkf20   = 0.005412309936 ;

/****************************************************************************
 * BT3 + BT4
 ****************************************************************************/
 dbtdvt30  = 0.002665654855 ;
 dbtdvt40  = 0.005498301687 ;

 /****************************************************************************
 * BT
 ****************************************************************************/
 kBTQNO10  = -0.66749;
 kBTQNO20  =  0.63160;
 kBTQNO30  = -0.28709;
 kBTQNO40  =  0.73355;
 kBTQNO50  = -0.73616;
 dBTDVT50  = 0;
 dBTDVT60  = 0;

 option, info;
 option, warn;
 option, echo;
 return;
