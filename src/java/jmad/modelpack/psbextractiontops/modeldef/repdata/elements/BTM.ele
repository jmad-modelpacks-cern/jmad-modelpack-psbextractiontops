option, echo;
option, -warn;
!--------------------------------------------------------------------------------------------
! - The lengths of BT.BHZ10, BTM.BHZ10, QNOs, DVT and DHZ is the magnetic length
!
! - For BT.BHZ10 and BTM.BHZ10, L corresponds to the beam path length
!
!--------------------------------------------------------------------------------------------
! Directory: /afs/cern.ch/eng/ps/cps/TransLines/PSB-PS/2014/elements
! Element file created in June 2014 by O.Berrig, G.P. Di Giovanni, V. Raginel and B. Mikulec
!--------------------------------------------------------------------------------------------

dBTBHZ10  = -0.162395099098; !  BT.BHZ10 bending angle from survey
dBTMBHZ10 = -0.350736762537; ! BTM.BHZ10 bending angle from survey
E1BTBHZ10 = 0 ; ! 0.0105;  ! The real value is 0.0105 rad; From J-M.LaCroix see: link on optics web ?! To be uploaded

! The HGAP should be measured on the magnet
! Magnetic length Lmag = 1.5271
BT.BHZ10      : SBEND,       L:=1.5271* dBTBHZ10/(sin(E1BTBHZ10)+sin(dBTBHZ10-E1BTBHZ10)),ANGLE:= dBTBHZ10,E1:=E1BTBHZ10,E2:= dBTBHZ10-E1BTBHZ10,FINT=0.3, HGAP=.07;
BTM.VVS10    :  MARKER;
BTM.BTV10    :  MARKER;
BTM.QNO05    :  QUADRUPOLE,  L= 0.56,  K1 := KBTMQNO05;  ! Magnetic length = 0.56, Physical length = 0.5
! Magnetic length Lmag=2.339
BTM.BHZ10    :  SBEND,       L:= 2.339 * dBTMBHZ10/(2*sin(dBTMBHZ10/2)) ,ANGLE := dBTMBHZ10, E1:= dBTMBHZ10/2, E2:=dBTMBHZ10/2, FINT=0.3, HGAP=.540;
BTM.QNO10    :  QUADRUPOLE,  L= 0.56,  K1 := KBTMQNO10;
BTM.DVT10    :  VKICKER,     L= 0.300, KICK  := dBTMDVT10;
BTM.DHZ10    :  HKICKER,     L= 0.300, KICK  := dBTMDHZ10;
BTM.QNO20    :  QUADRUPOLE,  L= 0.56,  K1 := KBTMQNO20;
BTM.BPM00    :  MARKER;
BTM.BPM10    :  MARKER;
BTM.BTV15    :  MARKER;
BTM.VPI11    :  MARKER;
BTM.VPI11A   :  MARKER;
BTM.BSGH01   :  MARKER;
BTM.BSGV01   :  MARKER;
BTM.BSGH02   :  MARKER;
BTM.BSGV02   :  MARKER;
BTM.BTV20    :  MARKER;
BTM.BSGH03   :  MARKER;
BTM.BSGV03   :  MARKER;
BTM.BCT10    :  MARKER;
BTM.TDU10    :  MARKER;

! This element is to be defined in the PSB-ISOLDE optics
! For the BTM line, this is a non active element
BTY.BVT101VC :  PLACEHOLDER;

return;
