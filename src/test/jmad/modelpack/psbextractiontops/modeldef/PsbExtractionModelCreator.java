/**
 * Copyright (c) 2018 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package jmad.modelpack.psbextractiontops.modeldef;

import static cern.accsoft.steering.jmad.tools.modeldefs.creating.ModelDefinitionCreator.scanDefault;

public class PsbExtractionModelCreator {
    
    public static void main(String[] args) {
        scanDefault().and().writeTo("src/java/jmad/modelpack/psbextractiontops/modeldef");
    }

}
