/**
 * Copyright (c) 2018 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package jmad.modelpack.psbextractiontops.modeldef;

import static cern.accsoft.steering.jmad.domain.file.CallableModelFile.ParseType.STRENGTHS;

import cern.accsoft.steering.jmad.tools.modeldefs.creating.lang.JMadModelDefinitionDslSupport;

public class PsbExtractionToDumpModelFactory extends JMadModelDefinitionDslSupport {

    {
        name("PSBTODUMP");

       

        init(i -> {
            i.call("strength/BT_BTP.str").parseAs(STRENGTHS);
            
            i.call("aperture/psb.dbx");
            i.call("elements/psb.ele");
            i.call("sequence/psb.seq");
            i.call("strength/psb_extraction.str").parseAs(STRENGTHS);
            i.call("strength/psb_orbit.str").parseAs(STRENGTHS);
            
            i.call("aperture/BT.dbx");
            i.call("elements/BT.ele");
            i.call("sequence/BT1.seq");
            i.call("sequence/BT2.seq");
            i.call("sequence/BT3.seq");
            i.call("sequence/BT4.seq");
            
            i.call("aperture/BTM.dbx");
            i.call("elements/BTM.ele");
            i.call("sequence/BTM.seq");
            i.call("strength/BTM_dump.str").parseAs(STRENGTHS);
            
            i.call("elements/extractionElements.ele");
            
            i.call("beam/psbbeam.beamx");
            
            i.call("strength/extraSeptaON.str");
            i.call("strength/extraKickerON.str");
            i.call("jmad/ring1/p14ring1ExtBendSeq.madx");
            i.call("jmad/ring1/psb1ExtrBendSeq.madx");
            i.call("jmad/ring2/p14ring2ExtBendSeq.madx");
            i.call("jmad/ring2/psb2ExtrBendSeq.madx");
            i.call("jmad/ring3/p14ring3ExtBendSeq.madx");
            i.call("jmad/ring3/psb3ExtrBendSeq.madx");
            i.call("jmad/ring4/p14ring4ExtBendSeq.madx");
            i.call("jmad/ring4/psb4ExtrBendSeq.madx");
            
            i.call("jmad/ring1/combineBoosterWithBt1Btm.madx");
            i.call("jmad/ring2/combineBoosterWithBt2Btm.madx");
            i.call("jmad/ring3/combineBoosterWithBt3Btm.madx");
            i.call("jmad/ring4/combineBoosterWithBt4Btm.madx");
            
            
            
            i.call("jmad/ring1/combineBt1Btm.madx");
            i.call("jmad/ring2/combineBt2Btm.madx");
            i.call("jmad/ring3/combineBt3Btm.madx");
            i.call("jmad/ring4/combineBt4Btm.madx");
        });
        


        
        sequence("boosterBT1btm").isDefault().isDefinedAs(s -> {
            s.range("ALL").isDefault().isDefinedAs(r -> {
                r.twiss(t -> {
                    t.betx(5.992549585);
                    t.alfx(0.2338292938);
                    t.bety(4.629954604);
                    t.alfy(0.3147816954);
                    t.dx(-1.467310299);
                    t.dpx(-6.287423904e-05);
                    t.calcAtCenter();
                });
            });
        });
        
        
        sequence("boosterBT2btm").isDefinedAs(s->{
            s.range("ALL").isDefault().isDefinedAs(r->{
                r.twiss(t->{
                    t.betx(5.992549491);
                    t.alfx(0.2338290603);
                    t.bety(4.629426542);
                    t.alfy(0.3146238805);
                    t.dx(-1.465850985);
                    t.dpx(5.150972083e-05);
                    t.calcAtCenter();
                });
            });
        });
        
        sequence("boosterBT3btm").isDefinedAs(s->{
            s.range("ALL").isDefault().isDefinedAs(r->{
                r.twiss(t->{
                    t.betx(5.992549491);  
                    t.alfx(0.2338290603);
                    t.bety(4.629426542);
                    t.alfy(0.3146238805);
                    t.dx(-1.465850985);
                    t.dpx(5.150972083e-05);
                    t.calcAtCenter();
                });
            });
        });
        
        
        sequence("boosterBT4btm").isDefinedAs(s->{
            s.range("ALL").isDefault().isDefinedAs(r->{
                r.twiss(t->{
                    t.betx(5.992549491);  
                    t.alfx(0.2338290603);
                    t.bety(4.629426542);
                    t.alfy(0.3146238805);
                    t.dx(-1.465850985);
                    t.dpx(5.150972083e-05);
                    t.calcAtCenter();
                });
            });
        });
        
        
        sequence("bt1btm").isDefinedAs(s->{
            s.range("ALL").isDefault().isDefinedAs(r->{
                r.twiss(t->{
                    t.betx(5.673289475);  
                    t.alfx(-0.02432259222);
                    t.bety(4.221078951);
                    t.alfy(-0.0510726752);
                    t.dx(-1.375827269);
                    t.dpx(0.05349973548);
                    t.calcAtCenter();
                });
            });
        });
        
        
        sequence("bt2btm").isDefinedAs(s->{
            s.range("ALL").isDefault().isDefinedAs(r->{
                r.twiss(t->{
                    t.betx(5.673289734);  
                    t.alfx(-0.02432258523);
                    t.bety(4.221064411);
                    t.alfy(-0.05107326185);
                    t.dx(-1.375961388);
                    t.dpx(0.05348009608);
                    t.calcAtCenter();
                });
            });
        });
        
        
        sequence("bt3btm").isDefinedAs(s->{
            s.range("ALL").isDefault().isDefinedAs(r->{
                r.twiss(t->{
                    t.betx(5.673289475);  
                    t.alfx(-0.02432259222);
                    t.bety(4.221078951);
                    t.alfy(-0.0510726752);
                    t.dx(-1.375827269);
                    t.dpx(0.05349973548);
                    t.calcAtCenter();
                });
            });
        });
        
        sequence("bt4btm").isDefinedAs(s->{
            s.range("ALL").isDefault().isDefinedAs(r->{
                r.twiss(t->{
                    t.betx(5.673289734);  
                    t.alfx(-0.02432258523);
                    t.bety(4.221064411);
                    t.alfy(-0.05107326185);
                    t.dx(-1.375961388);
                    t.dpx(0.05348009608);
                    t.calcAtCenter();
                });
            });
        });
        
        optics("allRings_PSB_BT_BTM_2018").isDefault().isDefinedAs(o -> {
            o.call("strength/psb_extraction.str").parseAs(STRENGTHS);
            o.call("strength/psb_orbit.str").parseAs(STRENGTHS);
            o.call("strength/extraSeptaON.str").parseAs(STRENGTHS);
            o.call("strength/extraKickerON.str").parseAs(STRENGTHS);
            o.call("strength/BT_dump.str").parseAs(STRENGTHS);
            o.call("strength/BTM_dump.str").parseAs(STRENGTHS);
        });

    }

}