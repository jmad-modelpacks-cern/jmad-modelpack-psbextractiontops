/**
 * Copyright (c) 2018 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package jmad.modelpack.psbextractiontops.modeldef;

import static cern.accsoft.steering.jmad.domain.file.CallableModelFile.ParseType.STRENGTHS;

import cern.accsoft.steering.jmad.tools.modeldefs.creating.lang.JMadModelDefinitionDslSupport;

public class PsbExtractionWithPsRingModelFactory extends JMadModelDefinitionDslSupport {

    {
        name("PSBTOPS_WITH_PSRING");

       

        init(i -> {
            i.call("strength/BT_BTP.str").parseAs(STRENGTHS);
            
            i.call("aperture/psb.dbx");
            i.call("elements/psb.ele");
            i.call("sequence/psb.seq");
            i.call("strength/psb_extraction.str").parseAs(STRENGTHS);
            i.call("strength/psb_orbit.str").parseAs(STRENGTHS);
            
            
            i.call("aperture/BT.dbx");
            i.call("elements/BT.ele");
            i.call("sequence/BT1.seq");
            i.call("sequence/BT2.seq");
            i.call("sequence/BT3.seq");
            i.call("sequence/BT4.seq");
            
            
            i.call("aperture/BTP.dbx");
            i.call("elements/BTP.ele");
            i.call("sequence/BTP.seq");
            i.call("strength/BTP.str").parseAs(STRENGTHS);
            
            i.call("aperture/BTM.dbx");
            i.call("elements/BTM.ele");
            i.call("sequence/BTM.seq");
            i.call("strength/BTM_dump.str").parseAs(STRENGTHS);
            
            i.call("elements/PS.ele");
            i.call("sequence/PS.seq");
            i.call("strength/PS_Injection7_for_OP_group.str");
            
            i.call("elements/extractionElements.ele");
            
            i.call("beam/psbbeam.beamx");
            
            i.call("jmad/ps/injectionBump.madx");
            i.call("jmad/ps/preparePsSequence.madx");
            
            i.call("strength/extraSeptaON.str");
            i.call("strength/extraKickerON.str");
            i.call("jmad/ring1/p14ring1ExtBendSeq.madx");
            i.call("jmad/ring1/psb1ExtrBendSeq.madx");
            i.call("jmad/ring1/combineBoosterWithBt1BtpPs.madx");
            i.call("jmad/ring2/p14ring2ExtBendSeq.madx");
            i.call("jmad/ring2/psb2ExtrBendSeq.madx");
            i.call("jmad/ring2/combineBoosterWithBt2BtpPs.madx");
            i.call("jmad/ring3/p14ring3ExtBendSeq.madx");
            i.call("jmad/ring3/psb3ExtrBendSeq.madx");
            i.call("jmad/ring3/combineBoosterWithBt3BtpPs.madx");
            i.call("jmad/ring4/p14ring4ExtBendSeq.madx");
            i.call("jmad/ring4/psb4ExtrBendSeq.madx");
            i.call("jmad/ring4/combineBoosterWithBt4BtpPs.madx");
           
        });
        


        sequence("psbbt1btpps").isDefault().isDefinedAs(s -> {
            s.range("ALL").isDefault().isDefinedAs(r -> {
                r.twiss(t -> {
                    t.betx(5.992549585);
                    t.alfx(0.2338292938);
                    t.bety(4.629954604);
                    t.alfy(0.3147816954);
                    t.dx(-1.467310299);
                    t.dpx(-6.287423904e-05);
                    t.calcAtCenter();
                });
            });
        });
        
        
        sequence("psbbt2btpps").isDefinedAs(s->{
            s.range("ALL").isDefault().isDefinedAs(r->{
                r.twiss(t->{
                    t.betx(5.992549491);
                    t.alfx(0.2338290603);
                    t.bety(4.629426542);
                    t.alfy(0.3146238805);
                    t.dx(-1.465850985);
                    t.dpx(5.150972083e-05);
                    t.calcAtCenter();
                });
            });
        });
        
       
        sequence("psbbt3btpps").isDefinedAs(s->{
            s.range("ALL").isDefault().isDefinedAs(r->{
                r.twiss(t->{
                    t.betx(5.992549491);  
                    t.alfx(0.2338290603);
                    t.bety(4.629426542);
                    t.alfy(0.3146238805);
                    t.dx(-1.465850985);
                    t.dpx(5.150972083e-05);
                    t.calcAtCenter();
                });
            });
        });
        
        
        sequence("psbbt4btpps").isDefinedAs(s->{
            s.range("ALL").isDefault().isDefinedAs(r->{
                r.twiss(t->{
                    t.betx(5.992549491);  
                    t.alfx(0.2338290603);
                    t.bety(4.629426542);
                    t.alfy(0.3146238805);
                    t.dx(-1.465850985);
                    t.dpx(5.150972083e-05);
                    t.calcAtCenter();
                });
            });
        });
        
        

        optics("allRings_PSB_BT_BTP_PS_2018").isDefault().isDefinedAs(o -> {
            o.call("strength/psb_extraction.str").parseAs(STRENGTHS);
            o.call("strength/psb_orbit.str").parseAs(STRENGTHS);
            o.call("strength/extraSeptaON.str").parseAs(STRENGTHS);
            o.call("strength/extraKickerON.str").parseAs(STRENGTHS);
            o.call("strength/BT_BTP.str").parseAs(STRENGTHS);
            o.call("strength/PS_Injection7_for_OP_group.str").parseAs(STRENGTHS);
        });
       

    }

}